# microfolielens

Application de suivi des activités de Microfolie Lens

Testé avec Eclipse (Dynamic Web Project, les fichier de config d'eclipse sont dans le git) et Tomcat 9

## Contexte

La municipalité de Lens a ouvert en début d'année un site Microfolie
contenant divers ateliers, en majorité liés au numérique :

- réalité virtuelle
- musée numérique
- cyber base
- shooting photo
- jeux vidéos
- espace scénique

Cette structure est ouverte à tous.

Actuellement, une personne souhaitant utiliser le matériel indique ses 
coordonnées à l'accueil, sur un registre papier.

## Besoin

La municipalité souhaite pouvoir analyser la fréquentation de cette structure,
notamment le profil des usagers, mais aussi celles des différents ateliers.
Cela permettrait de voir quels sont les espaces qui sont les plus fréquentés de manière visuelle. (graphiques, stats, ...)

Idéalement, les usagers devraient pouvoir disposer d'une carte d'accès à la 
microfolie lui permettant de s'identifier au niveau de la structure et des 
ateliers. Cette carte pourrait être matérielle (papier/carton) ou virtuelle 
(visible sur smartphone). 

La microfolie dispose d'un réseau Wifi propre, de machines fixes et de tablettes Android.
Une solution qui parait simple à mettre en oeuvre serait d'éditer des cartes 
personnelles avec un identifiant qui puisse être lu par les tablettes, disposées
à l'entrée de la microfolie et sur chaque atelier.

Un logiciel de création des cartes et de visualisation de la fréquentation serait
installé sur un poste fixe.

On pourrait aussi automatiser la création de carte pour les nouveaux utilisateurs.
La personne pourrait par exemple saisir les données sur une tablette et on pourrait générer et
imprimer des étiquettes (avec un code barre ou un QRCode, etc) pour les coller directement sur les cartes.

Cartes de groupes : lorsque l'on badge une carte de groupe, elle peut indiquer le nombre de personnes qu'elle accompagne.

On pourra proposer un système de mise à jour des cartes, de mise à jour des espaces (création ou suppression), etc.

### Données usager à collecter

- Nom (obligatoire non modifiable)
- Prénom (obligatoire non modifiable)
- Sexe (obligatoire, prévoir 3 choix)
- Date de naissance (obligatoire non modifiable)
- Ville (obligatoire modifiable)
- Situation scolaire (obligatoire établissement, ville, classe...) ou professionnelle (ville, libellé...)
- Email (au moins un des 2 obligatoires)
- Téléphone (au moins un des 2 obligatoires)
- Personne à contacter en cas d'urgence (obligatoire)
- Pièce jointe (photo, document)

**Attention à la RGPD**

### Exemples de statistiques demandées

- Courbes de fréquentation (journalières, hebdomadaires, mensuelles, annuelles) avec export Excel
- Profil des usagers

Propositions de métriques à afficher sur le tableau de bord

- fréquentation par an/mois/semaine/jour avec valeur médiane et détails disponibles par clic sur la mesure
- fréquentation des espaces paramétrable (journée, semaine, mois, année, autre)
- histogramme du nombre cumulé de fréquentations dans l'année (x = nombre d'inscrits, y=fréquentation) en utilisant éventuellement des intervalles pour y.
- portrait robot d'un usager : age median et sexe majoritaire

### Contraintes techniques

Idéalement, on devrait scanner les cartes à partir de l'appareil photo des tablettes. (Samsung Galaxy Tab 4, Androïd 5.0.2, capteur photo arrière : 3 Mpixels)

### Contraintes métiers

 - Une personne ne peut pas être comptabilisée plus d'une fois par jour pour l'accès à la microfolie ou sur une activité.
C'est-à-dire : dans la même journée, elle ne peut badger qu'une fois son entrée à tout le complexe, mais peut badger ensuite une fois par espace.

 - Deux types de cartes : cartes individuelles et cartes de groupe.

## Organisation du projet

+ Product Owner : Rachid Ouhassou
+ Scrum Master : Daniel Le Berre
+ Scrum Team : M2 ILI

+ Fin du projet : première semaine de décembre
+ Premier jalon : 23 octobre