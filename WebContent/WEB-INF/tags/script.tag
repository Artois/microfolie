<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/materialize.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cookie-consent.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/script.js"/>"></script>