<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute required="false" name="active" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

	<s:url var="inscriptionurl" action="gotoinscription"/>
    <s:url var="abonnesurl" action="gotoabonnes"/>
	<s:url var="settingsurl" action="gotosettings"/>
    <!-- Header Desktop -->
	<header class="navbar-fixed">
        <nav class="red">
            <div class="nav-wrapper">
                <a href="<c:url value="/"/>" class="brand-logo center mtop-half hide-on-med-and-down"><img src="<c:url value="/img/logo.png"/>"
                        title="Logo Microfolie" class="responsive-img" style="max-width: 128px;" /></a>
                <a href="<c:url value="/"/>" class="brand-logo center hide-on-large-only">Microfolie Lens</a>
                <a href="#!" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <div class="left mleft-2 hide-on-med-and-down">
                    <a href="<c:url value="/"/>">
                        <h5>Microfolie Lens</h5>
                    </a>
                </div>
                <ul class="right mright-2 hide-on-med-and-down">
                    <li <c:if test="${active == 'abonnes'}">class="active"</c:if>><a href="${abonnesurl}">Abonn&eacute;s</a></li>
                    <li <c:if test="${active == 'inscription'}">class="active"</c:if>><a href="${inscriptionurl}">Inscription</a></li>
                    <li <c:if test="${active == 'settings'}">class="active"</c:if>><a href="${settingsurl}">Param&eacute;trage espace</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Header Mobile -->
    <ul class="sidenav" id="mobile-nav">
        <li class="center-align mtop-1">
            <img src="<c:url value="/img/logo.png" />" title="Logo Microfolie" class="responsive-img" style="max-height: 150px;" />
        </li>
        <li class="center-align">
            <h4>Microfolie Lens</h4>
            <hr />
        </li>
        <li <c:if test="${active == 'abonnes'}'">class="active"</c:if>><a href="${abonnesurl}">Abonn&eacute;s</a></li>
        <li <c:if test="${active == 'inscription'}">class="active"</c:if>><a href="${inscriptionurl}">Inscription</a></li>
        <li <c:if test="${active == 'settings'}">class="active"</c:if>><a href="${settingsurl}">Param&eacute;trage espace</a></li>
    </ul>
    <div class="mtop-5 hide-on-med-and-down"></div>