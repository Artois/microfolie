<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute required="true" name="title" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/css/material-icons.css"/>" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css"/>" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/css/style.min.css"/>" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/css/cookie-consent.min.css"/>" />
    <link rel="icon" href="<c:url value="/img/favicon.ico"/>" />
    <title>${title} - Microfolie Lens</title>