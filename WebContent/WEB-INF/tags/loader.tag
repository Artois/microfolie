<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute required="true" name="name" type="java.lang.String" %>

<style>
	#${name}main {
	  position: absolute;
	  top: calc(50% - 20px);
	  left: calc(50% - 20px);
	}
	@keyframes ${name}main {
	  0% { left: -100px }
	  100% { left: 110%; }
	}
	#${name}box {
	  width: 50px;
	  height: 50px;
	  background: #fff;
	  animation: animate .5s linear infinite;
	  position: absolute;
	  top: 0;
	  left: 0;
	  border-radius: 3px;
	}
	@keyframes animate {
	  17% { border-bottom-right-radius: 3px; }
	  25% { transform: translateY(9px) rotate(22.5deg); }
	  50% {
	    transform: translateY(18px) scale(1,.9) rotate(45deg) ;
	    border-bottom-right-radius: 40px;
	  }
	  75% { transform: translateY(9px) rotate(67.5deg); }
	  100% { transform: translateY(0) rotate(90deg); }
	} 
	#${name}shadow { 
	  width: 50px;
	  height: 5px;
	  background: #000;
	  opacity: 0.1;
	  position: absolute;
	  top: 59px;
	  left: 0;
	  border-radius: 50%;
	  animation: shadow .5s linear infinite;
	}
	@keyframes ${name}shadow {
	  50% {
	    transform: scale(1.2,1);
	  }
	}
</style>

<!-- Loader by Fabrizio Bianchi: https://codepen.io/_fbrz -->
<!-- Loader pen : https://codepen.io/_fbrz/pen/mpiFE -->
<div id="${name}" class="modal loader red">
    <div class="modal-content" style="overflow: hidden;">
    	<div class="row">
    		<div class="col s12 center-align">
    			<h4 class="white-text">Chargement</h4>
    		</div>
    	</div>
    	<div class="row mtop-5 mbot-5">
    		<div class="col s12">
    			<div id="${name}main">
				  	<div id="${name}shadow"></div>
				  	<div id="${name}box"></div>
				</div>
    		</div>
    	</div>
	</div>
</div>
