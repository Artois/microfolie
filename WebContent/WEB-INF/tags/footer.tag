<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<footer class="page-footer red">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Microfolie Lens</h5>
                    <p class="grey-text text-lighten-4">Bla bla bla ...</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Liens utiles</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="<c:url value="/" />">Accueil</a></li>
                        <li><a class="grey-text text-lighten-3" href="#apropos">Qui somme nous</a></li>
                        <li><a class="grey-text text-lighten-3" href="#mention">Mentions l&eacute;gales</a></li>
                        <li><a class="grey-text text-lighten-3" href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Copyright Text
                <a class="grey-text text-lighten-4 right" href="https://villedelens.fr" target="_blank">Ville de
                    Lens</a>
            </div>
        </div>
    </footer>