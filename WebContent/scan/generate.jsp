<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <tag:head title="QR Code" />
    <style>
    	img {
    		margin-left: auto;
    		margin-right: auto;
    	}
    </style>
</head>

<body class="grey lighten-3">
    <!-- Header -->
    <header class="navbar-fixed">
        <nav class="red">
            <div class="nav-wrapper">
                <a href="<c:url value="/" />" class="brand-logo center mtop-half hide-on-med-and-down"><img src="../img/logo.png"
                        title="Logo Microfolie" class="responsive-img" style="max-width: 128px;" /></a>
                <a href="<c:url value="/" />" class="brand-logo center hide-on-large-only">Microfolie Lens</a>
                <div class="left mleft-2 hide-on-med-and-down">
                    <span>
                        <h5>Microfolie Lens</h5>
                    </span>
                </div>
            </div>
        </nav>
    </header>
    <div class="mtop-5 hide-on-med-and-down"></div>
    
    <!-- Main -->
    <main class="container">
    	<div class="row mtop-2">
    		<div class="col s12">
    			<a onclick="window.history.back()" class="waves-effect waves-light btn" >
    				<i class="material-icons left">arrow_back</i>Retour
    			</a>
    		</div>
    	</div>
    	<div class="white z-depth-1">
	    	<div class="row">
	    		<div class="col s12 center-align">
	    			<h3>Code : <span id="code"></span></h3>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col s12">
	    			<div id="qrcode"></div>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col s12 center-align" style="padding-bottom: 1em">
	    			<a href="#"
						onclick="this.href = $('#qrcode img:first').attr('src');"
						class="waves-effect waves-light btn-large" download="qrcode.png">
	    				<i class="material-icons left">keyboard_arrow_down</i>T&eacute;l&eacute;charger
	    			</a>
	    		</div>
	    	</div>
    	</div>
    </main>
    
    <!-- Footer -->
    <tag:footer />
    
    <!-- Script -->
    <tag:script />
    <script type="text/javascript" src="<c:url value="/js/qrcode.min.js" />"></script>
    <script type="text/javascript">
    	$(document).ready(() => {
    		const params = new URLSearchParams(window.location.search);
    		if (!params.has("code")) {
    			window.location.href = '../';
    		}
    		$("#code").html(params.get("code"));
    		new QRCode("qrcode", params.get("code"));
    	});
    </script>
    
</body>
</html>