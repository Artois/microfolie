<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.List" %>
<%@ page import="microfolie.service.EspaceService" %>
<%@ page import="microfolie.service.dto.EspaceDTO" %>

<%
	EspaceService espaceService = EspaceService.getInstance();
	List<EspaceDTO> espaces = espaceService.getAll();
%>

<!DOCTYPE html>
<html>

<head>
    <tag:head title="Paramétrage" />
</head>

<body class="grey lighten-3">
    <!-- Header -->
    <tag:header active="settings" />

    <!-- Main -->
    <main class="container">
        <div class="row">
            <div class="col s12 center-align">
                <h3>Param&eacute;trage</h3>
            </div>
        </div>
        <div class="row center-align">
        	<c:forEach items="<%= espaces %>" var="espace">
	        	<div class="col l4 m6 s12 mbot-5">
	                <a href="<c:url value="/settings/configure?espace=${espace.code}"/>" class="waves-effect waves-light btn-large red">${espace.libelle}</a>
	            </div>
        	</c:forEach>
        </div>
    </main>

    <!-- Footer -->
    <tag:footer />

    <!-- Script -->
    <tag:script />
</body>

</html>