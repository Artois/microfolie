<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />

<!DOCTYPE html>
<html>

<head>
    <tag:head title="Abonnés" />
</head>

<body class="grey lighten-3">
    <!-- Header -->
    <tag:header active="abonnes" />

    <!-- Main -->
    <main class="container">
        <div class="row">
            <div class="col s12 center-align">
                <h3>Abonn&eacute;s</h3>
            </div>
        </div>
        <div class="row">
            <div id="abo" class="col s12 center-align"></div>
        </div>
        <div class="row">
            <div id="pagination-abo" class="col s12 center-align"></div>
        </div>
    </main>

    <!-- Footer -->
    <tag:footer />
    
    <!-- Confirmation -->
  	<div id="confirmation" class="modal conf">
    	<div class="modal-content">
    		<div class="row">
    			<div class="col offset-m1 m3 mtop-4">
                    <i class="material-icons red-text" style="font-size: 8em;">warning</i>
                </div>
                <div class="col m7 s12 mtop-3">
                    <h4>Confirmer la suppression de <span id="conf-user"></span> ?</h4>
      				<p style="font-size:1.2em">La suppression est d&eacute;finitive et aucun retour en arri&egrave;re n'est possible</p>
                </div>
    		</div>
    	</div>
    	<div class="modal-footer">
      		<a href="#!" class="modal-close waves-effect waves-red btn-flat">Annuler</a>
      		<a href="#!" id="conf-delete" class="modal-close waves-effect waves-light btn">Confirmer</a>
    	</div>
  	</div>
    <!-- Popup -->
    <div id="ok-popup" class="modal popup">
        <div class="modal-content" style="overflow: hidden;">
            <div class="row">
                <div class="col offset-m1 m3 s12">
                    <i class="material-icons green-text" style="font-size: 8em;">check_circle</i>
                </div>
                <div class="col m7 s12 mtop-3">
                    <h4 id="ok-text">Success message</h4>
                </div>
            </div>    
        </div>
    </div>
    <div id="err-popup" class="modal popup">
        <div class="modal-content" style="overflow: hidden;">
            <div class="row">
                <div class="col offset-m1 m3 s12">
                    <i class="material-icons red-text" style="font-size: 8em;">error</i>
                </div>
                <div class="col m7 s12 mtop-3">
                    <h4 id="error-text">Error message</h4>
                </div>
            </div>    
        </div>
    </div>
    <!--  Loader  -->
    <tag:loader name="loader" />

    <!-- Script -->
    <tag:script />
    <script type="text/javascript">
        $(document).ready(() => {
        	const loader = M.Modal.getInstance($('#loader'));
        	const okPopup = M.Modal.getInstance($('#ok-popup'));
        	const errPopup = M.Modal.getInstance($('#err-popup'));
        	const conf = M.Modal.getInstance($('#confirmation'));
        	const idPagination = 'usager';
        	const perPage = 5;
        	var page = 1;
        	
        	function pagine(numPage) {
        		numPage = numPage || page;
        		loader.open();
                $.ajax({
                    type: "GET",
                    url: "${base}api/usager/page/" + numPage + "/" + perPage + "/formatted/pagination",
                    error: () => {
                    	loader.close();
                    	pagination(idPagination, numPage, perPage, 0, $('#abo'), $('#pagination-abo'), []);
                		let html = '<tr>';
                		html += '<td class="right-align red-text" width="40%"><i class="material-icons" style="font-size:4em;padding-top:.3em;">error</i></td>';
                		html += '<td class="left-align red-text" width="60%"><h4>Impossible de charger les usager</td>';
                		html += '</tr>';
                		$('#pagination-' + idPagination + '-table').html(html);
                    },
                    success: (result) => {
                    	loader.close();
                    	if (result.data.list.length > 0) {
                    		pagination(idPagination, numPage, perPage, result.data.total, $('#abo'), $('#pagination-abo'), result.data.list, 'responsive-table striped', 'action');
                        	$('.tooltipped').tooltip();
                    	} else {
                    		pagination(idPagination, numPage, perPage, 0, $('#abo'), $('#pagination-abo'), []);
                    		let html = '<tr>';
                    		html += '<td class="right-align blue-text" width="40%"><i class="material-icons" style="font-size:4em;padding-top:.3em;">info</i></td>';
                    		html += '<td class="left-align blue-text" width="60%"><h4>Aucun usager</td>';
                    		html += '</tr>';
                    		$('#pagination-' + idPagination + '-table').html(html);
                    	}
                    },
                });
        	};
        	
			pagine(page);
            // Detection changement page
            $('main').on('click', '.pagination-' + idPagination + '-prev', function () {
                console.log(idPagination, 'prev', $(this).attr('data-page'));
                page = $(this).attr('data-page');
                pagine(page);
            });
            $('main').on('click', '.pagination-' + idPagination + '-next', function () {
                console.log('abo', 'next', $(this).attr('data-page'));
                page = $(this).attr('data-page');
                pagine(page);
            });
            $('main').on('click', '.pagination-' + idPagination + '-number', function () {
                console.log('abo', 'num', $(this).attr('data-page'));
                page = $(this).attr('data-page');
                pagine(page);
            });
            
            // Suppr abonne
            $('#abo').on('click', '.delete-usager', function () {
            	$("#conf-user").html($(this).attr('data-usager'));
            	$("#conf-delete").attr('data-code', $(this).attr('data-code'));
            	conf.open();
            });
            $('#conf-delete').on('click', function () {
            	loader.open();
            	$.ajax({
                    type: "DELETE",
                    url: "${base}api/usager/" + $(this).attr('data-code'),
                    error: () => {
                    	loader.close();
                    	$('#error-text').html('Une erreur est survenue');
                    	errPopup.open();
                    },
                    success: (result) => {
                    	loader.close();
                    	if (result.success) {
                    		$('#ok-text').html(result.data.msg);
                    		okPopup.open();
                    		pagine();
                    	} else {
                    		$('#error-text').html(result.data);
                        	errPopup.open();
                    	}
                    },
                });
            })
        });
    </script>
</body>

</html>