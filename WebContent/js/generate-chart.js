function randomColor() {
    return {
        r: Math.floor(Math.random() * 256),
        g: Math.floor(Math.random() * 256),
        b: Math.floor(Math.random() * 256)
    };
}

function generateBarChart(elt, title, colname, data) {
    let dataset = []
    data.forEach(element => {
        let color = element.color ? element.color : randomColor();
        let chartData = {
            label: element.title,
			backgroundColor: `rgba(${color.r}, ${color.g}, ${color.b}, 0.2)`,
			borderColor: `rgb(${color.r}, ${color.g}, ${color.b})`,
			borderWidth: 1,
			data: element.value
        };
        if(element.line !== undefined && element.line) {
            chartData['type'] = 'line';
            chartData['fill'] = false;
        }
        dataset.push(chartData);
    });

    ctx = elt.getContext('2d');
	return new Chart(ctx, {
        type: 'bar',
        data: {
            labels: colname,
            datasets: dataset
        },
        options: {
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: title
            },
            scales: {
            	yAxes: [{
            		ticks: {
            			beginAtZero: true
            		}
            	}]
            }
        }
    });
}