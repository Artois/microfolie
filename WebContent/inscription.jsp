<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />

<!DOCTYPE html>
<html>

<head>
    <tag:head title="Inscription" />
    <s:head />
</head>

<body class="grey lighten-3">
    <!-- Header -->
    <tag:header active="inscription" />

    <!-- Main -->
    <main class="container">
        <div class="row">
            <div class="col s12 center-align">
                <h3>Inscription</h3>
            </div>
        </div>
        <div class="row">
        	<!--s:debug /-->
            <s:form action="inscription" validate="true" class="col s12" autocomplete="off">
                <div class="row">
                    <div class="input-field col m6 s12">
                    	<i class="material-icons prefix">person</i>
                        <s:textfield id="pnom" name="prenom" class="validate" required="required" />
                        <label for="pnom">Pr&eacute;nom*</label>
                        <span class="helper-text"></span>
                        <s:fielderror fieldName="prenom" class="field-error hide" />
                    </div>
                    <div class="input-field col m6 s12">
                    	<i class="material-icons prefix">account_circle</i>
                        <s:textfield id="nom" name="nom" class="validate"  required="required" />
                        <label for="nom">Nom*</label>
                        <span class="helper-text"></span>
                        <s:fielderror fieldName="nom" class="field-error hide" />
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col m6 s12">
                    	<i class="material-icons prefix">wc</i>
                        <s:select id="genre" name="genre" list="getGenres()" headerKey="" headerValue="Selectionnez une valeur" required="required" />
                        <label for="genre">Genre*</label>
                        <s:fielderror fieldName="genre" class="red-text" style="padding-left: 3em" />
                    </div>
                    <div class="input-field col m6 s12">
                        <i class="material-icons prefix">cake</i>
                        <s:textfield id="naissance" name="naissance" class="validate datepicker"  required="required" />
                        <label for="naissance">Date de naissance*</label>
                        <span class="helper-text"></span>
                        <s:fielderror fieldName="naissance" class="field-error hide" />
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col m6 s12">
                        <i class="material-icons prefix">location_city</i>
                        <s:textfield id="ville" name="ville" class="validate autocomplete-ville"  required="required" />
                        <label for="ville">Ville*</label>
                        <span class="helper-text"></span>
                        <s:fielderror fieldName="ville" class="field-error hide" />
                    </div>
                    <div class="input-field col m6 s12">
                        <i class="material-icons prefix">work</i>
                        <s:textfield id="situation" name="situation" class="validate autocomplete-ecole"  required="required" />
                        <label for="situation">&Eacute;cole*</label>
                        <span class="helper-text"></span>
                        <s:fielderror fieldName="situation" class="field-error hide" />
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col m6 s12">
                        <i class="material-icons prefix">email</i>
                        <s:textfield id="email" name="email" class="validate"  required="required" />
                        <label for="email">Email*</label>
                        <span class="helper-text"></span>
                        <s:fielderror fieldName="email" class="field-error hide" />
                    </div>
                    <div class="input-field col m6 s12">
                        <i class="material-icons prefix">local_phone</i>
                        <s:textfield id="telephone" name="telephone" class="validate"  required="required" />
                        <label for="telephone">T&eacute;l&eacute;phone*</label>
                        <span class="helper-text"></span>
                        <s:fielderror fieldName="telephone" class="field-error hide" />
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col m6 s12">
                        <i class="material-icons prefix">healing</i>
                        <s:textfield id="urgence" name="urgence" class="validate"  required="required" />
                        <label for="urgence">Contact d'urgence*</label>
                        <span class="helper-text"></span>
                        <s:fielderror fieldName="urgence" class="field-error hide" />
                    </div>
                    <div class="file-field input-field col m6 s12">
                        <div class="waves-effect waves-light btn red">
                            <i class="material-icons">cloud_upload</i>
                            <input id="pj" type="file">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Pi&egrave;ce jointe">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 center-align">
                    	<s:submit id="btn-valid" class="btn waves-effect waves-light btn red" value="Valider" />
                    </div>
                </div>
            </s:form>
        </div>
    </main>
    
    <!-- Loader -->
    <tag:loader name="loader"/>

    <!-- Footer -->
    <tag:footer />

    <!-- Script -->
    <tag:script />
    <script type="text/javascript">
    	function loadAutocomplete(source, callback = null) {
    		$.ajax({
                type: "GET",
                url: "${base}api/" + source + "/list/all/autocomplete",
                error: () => {
					console.error("Impossible de charger l'autocomplete");
					if (callback != null) {
						callback();
					}
                },
                success: (result) => {
                	$('input.autocomplete-' + source).autocomplete({
                		data: result.data,
                		limit: 5
                	});
                	if (callback != null) {
						callback();
					}
                },
            });
    	};
    
    	$(document).ready(() => {
    		const loader = M.Modal.getInstance($('#loader'));
    		
    		// Chargement autcomplete
    		loader.open();
    		loadAutocomplete('ville', () => {
    			loadAutocomplete('ecole', () => {
    				loader.close();
    			})
    		});
    		
    	});
    </script>
</body>

</html>