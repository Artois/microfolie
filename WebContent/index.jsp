<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />

<!DOCTYPE html>
<html>

<head>
    <tag:head title="Accueil" />
</head>

<body class="grey lighten-3">
    <!-- Header -->
    <tag:header />

    <!-- Main -->
    <main class="container">
        <div class="row">
            <div class="col s12 center-align">
                <h3>Dashboard</h3>
            </div>
        </div>
        <div class="row">
            <div class="col m12 l8 offset-l2">
                <canvas id="chart1"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col m12 l8 offset-l2">
                <canvas id="chart2"></canvas>
            </div>
        </div>
        <div class="row mtop-2">
            <div class="col m12 l6">
                <canvas id="chart3"></canvas>
            </div>
            <div class="col m12 l6">
                <canvas id="chart4"></canvas>
            </div>
        </div>
    </main>

    <!-- Footer -->
    <tag:footer />
    
    <!-- Loader -->
    <tag:loader name="loader" />

    <!-- Script -->
    <tag:script />
    <script type="text/javascript" src="js/chart.min.js"></script>
    <script type="text/javascript" src="js/generate-chart.js"></script>
    <script type="text/javascript">
    	function weekChart(id, callback = null) {
   		 	$.ajax({
                type: "GET",
                url: "${base}api/frequentation/stats/week",
                error: () => {
					console.error("Impossible de charger ageChart");
					if (callback != null) {
              			callback();
              	 	}
                },
                success: (result) => {
                	const data = result.data;
               	 	generateBarChart(
                    	document.getElementById(id),
                       	data.title,
                       	["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
                       	data.stats
                    );
               	 if (callback != null) {
               		 callback();
               	 }
                },
            });
   		};
    	
    	function ageChart(id, callback = null) {
    		 $.ajax({
                 type: "GET",
                 url: "${base}api/frequentation/stats/age",
                 error: () => {
					console.error("Impossible de charger ageChart");
					if (callback != null) {
               			callback();
               	 	}
                 },
                 success: (result) => {
                	 const data = result.data;
                	 generateBarChart(
                     	document.getElementById(id),
                        data.title,
                        data.labels,
                        data.stats
                     );
                	 if (callback != null) {
                		 callback();
                	 }
                 },
             });
    	};
    	
    	function espaceWeekChart(id, callback = null) {
   		 	$.ajax({
                type: "GET",
                url: "${base}api/frequentation/stats/espace/week",
                error: () => {
					console.error("Impossible de charger ageChart");
					if (callback != null) {
              			callback();
              	 	}
                },
                success: (result) => {
                	const data = result.data;
               	 	generateBarChart(
                    	document.getElementById(id),
                       	data.title,
                       	data.labels,
                       	data.stats
                    );
               	 if (callback != null) {
               		 callback();
               	 }
                },
            });
   		};
   		
   		function espaceMonthChart(id, callback = null) {
   		 	$.ajax({
                type: "GET",
                url: "${base}api/frequentation/stats/espace/month",
                error: () => {
					console.error("Impossible de charger ageChart");
					if (callback != null) {
              			callback();
              	 	}
                },
                success: (result) => {
                	const data = result.data;
               	 	generateBarChart(
                 	   	document.getElementById(id),
                       	data.title,
                       	data.labels,
                       	data.stats
                    );
               	 if (callback != null) {
               		 callback();
               	 }
                },
            });
   		};
    
        $(document).ready(() => {
        	const loader = M.Modal.getInstance($('#loader'));
        	loader.open();
        	weekChart("chart1", () => {
        		ageChart("chart2", () => {
        			espaceWeekChart("chart3", () => {
        				espaceMonthChart("chart4", () => {
        					loader.close();
        				})
        			})
        		});
        	})
            
        });
    </script>
</body>

</html>