
# INTRODUCTION

## PRESENTATION DE L’ENTREPRISE 

## LE CONTEXTE 

##  OBJET 

##  DESCRIPTION DE LA PRESTATION 

# DESCRIPTION DE L'EXISTANT 

## ORGANISATION 

## LE PROCESSUS METIER EXISTANT 

## INFRASTRUCTURE 

### Poste utilisateur 

### Progiciel  

### Base de données 

### Serveur 

### Imprimante 

### Scanner 

### Autres

# BESOINS FONCTIONNELS 

# LA PROPOSITION COMMERCIALE ATTENDUE 

## UNE PRESENTATION FONCTIONNELLE DE L’OFFRE 
## UNE PRESENTATION TECHNIQUE DE L’OFFRE

### Préconisations  

### Contrainte d'exploitation 

##  UN VOLET TARIFAIRE DE L’OFFRE

## CRITERES D’ANALYSE DE L’OFFRE

## LES LIVRABLES 

## LES MODALITES CONTRACTUELLES 

### Contrat 

### Licences 

### Évolution des prix

### Engagements et pénalités 

### Facturation 

### Publicité/Référence

### Conformité aux réglementations applicables 

### Règlement intérieur

### Garantie 

## DATE LIMITE ET MODALITE D’ENVOI DE LA PROPOSITION COMMERCIALE

## LE CALENDRIER DE LA CONSULTATION

## PRESENTATION ET DEMONSTRATION

## VISITE DE REFERENCES CLIENTS 

# ANNEXES

## PLANNING
## Clause de confidentialité