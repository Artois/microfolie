package microfolie.entry.form;

import java.util.Date;

import com.opensymphony.xwork2.ActionSupport;

import microfolie.service.UsagerService;
import microfolie.service.dto.UsagerDTO;

public class Inscription extends ActionSupport {

	private static final long serialVersionUID = -8872053158311741161L;
	private static final UsagerService service = UsagerService.getInstance();
	
	private String nom;
	private String prenom;
	private String genre;
	private Date naissance;
	private String ville;
	private String situation;
	private String email;
	private String telephone;
	private String urgence;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String[] getGenres() {
		return new String[] {"Homme", "Femme", "Autre" };
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public Date getNaissance() {
		return naissance;
	}
	
	public void setNaissance(Date naissance) {
		this.naissance = naissance;
	}
	
	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getSituation() {
		return situation;
	}

	public void setSituation(String situation) {
		this.situation = situation;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getUrgence() {
		return urgence;
	}

	public void setUrgence(String urgence) {
		this.urgence = urgence;
	}

	@Override
	public String execute() throws Exception {
		ville = ville.substring(0, 1).toUpperCase() + ville.substring(1).toLowerCase();
		situation = situation.substring(0, 1).toUpperCase() + situation.substring(1).toLowerCase();
		UsagerDTO usager = new UsagerDTO(nom, prenom, genre, naissance, ville, situation, email, telephone, urgence);
		service.add(usager);
		return SUCCESS;
	}

	public String before() throws Exception {
		return SUCCESS;
	}

}
