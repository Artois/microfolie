package microfolie.entry.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Configure
 */
@WebServlet(description = "Configuration Servlet", urlPatterns = { "/settings/configure" })
public class Configure extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6397356110836610964L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Configure() {
		super();
 	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getServletContext().log("Begin : Configuration");
		String espace = request.getParameter("espace");
		String page;

		/**
		 * go back to setting
		 */
 		if (espace == null) {
 			request.getServletContext().log("No GET parameter espace");
			page = "/settings/";
		} else {
			request.getServletContext().log("GET parameter espace = " + espace);
			Cookie cookie = new Cookie("microfolies.lens.espace", espace);
			cookie.setMaxAge(60 * 60 * 24 * 365);
			page = "/scan/";
			response.addCookie(cookie);
		}
 		request.getServletContext().log("End : Configuration");
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
