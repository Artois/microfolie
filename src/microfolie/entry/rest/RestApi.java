package microfolie.entry.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/api")
public class RestApi extends ResourceConfig {               

    public RestApi() {
        packages("rest");
    }
    
}