package microfolie.entry.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.json.JSONArray;
import org.json.JSONObject;

import microfolie.service.EspaceService;
import microfolie.service.dto.EspaceDTO;
import microfolie.utils.JsonUtils;

@Path("/espace")
@Produces("application/json")
public class EspaceController {
	
	private static final Logger LOGGER = Logger.getLogger(EspaceController.class.getName());
	
	private EspaceService service = EspaceService.getInstance();
	
	@GET
	@Path("/list/all")
	public String listAll() {
		LOGGER.info("Begin: Espace list all");
		List<EspaceDTO> espaces = service.getAll();
		JSONArray data = new JSONArray(espaces);
		LOGGER.info("End: Espace list all");
		return JsonUtils.success(data).toString();
	}
	
	@GET
	@Path("/get/{code}")
	public String getByCode(@PathParam("code") String code) {
		LOGGER.info("Begin: Espace get by code (" + code + ")");
		EspaceDTO espace = service.getByCode(code);
		JSONObject data = new JSONObject(espace);
		LOGGER.info("End: Espace get by code");
		return JsonUtils.success(data).toString();
	}

}
