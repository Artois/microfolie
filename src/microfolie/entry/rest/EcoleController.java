package microfolie.entry.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.json.JSONArray;
import org.json.JSONObject;

import microfolie.service.EcoleService;
import microfolie.service.dto.EcoleDTO;
import microfolie.utils.JsonUtils;

@Path("/ecole")
@Produces("application/json")
public class EcoleController {
	
	private static final Logger LOGGER = Logger.getLogger(BadgeController.class.getName());
	
	private EcoleService service = EcoleService.getInstance();
	
	@GET
	@Path("/list/all")
	public String getAll() {
		JSONArray data = new JSONArray();
		LOGGER.info("Begin: Ecole list all");
		List<EcoleDTO> ecoles = service.getAll();
		ecoles.forEach(elt -> data.put(elt.getLibelle()));
		LOGGER.info("End: Ecole list all");
		return JsonUtils.success(data).toString();
	}
	
	@GET
	@Path("/list/all/autocomplete")
	public String getAllAutocomplete() {
		JSONObject data = new JSONObject();
		LOGGER.info("Begin: Ecole list all autocomplete");
		List<EcoleDTO> ecoles = service.getAll();
		data.put("Aucun", "");
		ecoles.forEach(elt -> data.put(elt.getLibelle(), ""));
		LOGGER.info("End: Ecole list all autocomplete");
		return JsonUtils.success(data).toString();
	}
	
}
