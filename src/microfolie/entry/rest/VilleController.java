package microfolie.entry.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.json.JSONArray;
import org.json.JSONObject;

import microfolie.service.VilleService;
import microfolie.service.dto.VilleDTO;
import microfolie.utils.JsonUtils;

@Path("/ville")
@Produces("application/json")
public class VilleController {
	
private static final Logger LOGGER = Logger.getLogger(VilleController.class.getName());
	
	private VilleService service = VilleService.getInstance();
	
	@GET
	@Path("/list/all")
	public String listAll() {
		LOGGER.info("Begin: Ville list all");
		List<VilleDTO> villes = service.getAll();
		JSONArray data = new JSONArray(villes);
		LOGGER.info("End: Ville list all");
		return JsonUtils.success(data).toString();
	}
	
	@GET
	@Path("/list/all/autocomplete")
	public String listAllAutocomplete() {
		LOGGER.info("Begin: Ville list all autocomplete");
		List<VilleDTO> villes = service.getAll();
		JSONObject data = new JSONObject();
		villes.forEach(elt -> data.put(elt.getLibelle(), ""));
		LOGGER.info("End: Ville list all autocomplete");
		return JsonUtils.success(data).toString();
	}

}
