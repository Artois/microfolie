package microfolie.entry.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.json.JSONArray;
import org.json.JSONObject;

import microfolie.service.UsagerService;
import microfolie.service.dto.UsagerDTO;
import microfolie.utils.JsonUtils;

@Path("/usager")
@Produces("application/json")
public class UsagerController {
	
	private static final Logger LOGGER = Logger.getLogger(EspaceController.class.getName());
	
	private UsagerService service = UsagerService.getInstance();
	
	@GET
	@Path("/page/{num}/{perPage}")
	public String page(@PathParam("num") int num, @PathParam("perPage") int perPage) {
		LOGGER.info("Begin: Usager page (num: " + num + ", perPage: " + perPage + ")");
		JSONObject data = new JSONObject();
		data.put("total", service.getTotalNumber());
		data.put("list", new JSONArray(service.getPage(num, perPage)));
		LOGGER.info("End: Usager page");
		return JsonUtils.success(data).toString();
	}
	
	@GET
	@Path("/page/{num}/{perPage}/formatted/pagination")
	public String pageForPagination(@PathParam("num") int num, @PathParam("perPage") int perPage) {
		LOGGER.info("Begin: Usager page (num: " + num + ", perPage: " + perPage + ")");
		JSONObject data = new JSONObject();
		// Récupération info
		long total = service.getTotalNumber();
		if (num < 1) {
			num = 1;
		}
		while (total <= perPage * (num - 1)) {
			num--;
		}
		List<UsagerDTO> usagers = service.getPage(num, perPage);
		// Formattage des données
		data.put("total", total);
		JSONArray list = new JSONArray();
		usagers.forEach(elt -> {
			StringBuilder action = new StringBuilder();
			action.append("<a href='scan/generate.jsp?code=");
			action.append(elt.getCode());
			action.append("' class='tooltipped' data-position='right' data-tooltip='Voir le QR Code'><i class='material-icons'>center_focus_strong</i></a>");
			action.append("<i class='delete-usager material-icons red-text tooltipped mleft-1' data-code='");
			action.append(elt.getCode());
			action.append("' data-usager='");
			action.append(elt.getPrenom() + " " + elt.getNom());
			action.append("' data-position='right' data-tooltip='Supprimer usager' style='cursor: pointer'>delete_forever</i>");
			JSONObject json = new JSONObject();
			json.put("nom", elt.getNom());
			json.put("prenom", elt.getPrenom());
			json.put("email", elt.getEmail());
			json.put("ville", elt.getVille());
			json.put("action", action.toString());
			list.put(json);
		});
		data.put("list", list);
		LOGGER.info("End: Usager page");
		return JsonUtils.success(data).toString();
	}
	
	@DELETE
	@Path("/{code}")
	public String delete(@PathParam("code") String code) {
		JSONObject result;
		LOGGER.info("Begin: Usager delete (" + code + ")");
		if (service.delete(code)) {
			JSONObject data = new JSONObject();
			data.put("msg", "Usager supprim&eacute;");
			result = JsonUtils.success(data);
		} else {
			result = JsonUtils.error("Erreur lors de la suppression de l'usager");
		}
		LOGGER.info("End: Usager delete");
		return result.toString();
	}

}
