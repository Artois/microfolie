package microfolie.entry.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.json.JSONArray;
import org.json.JSONObject;

import microfolie.service.FrequentationService;
import microfolie.service.dto.FrequentationDTO;
import microfolie.utils.JsonUtils;

@Path("/frequentation")
@Produces("application/json")
public class FrequentationController {

	private static final Logger LOGGER = Logger.getLogger(FrequentationController.class.getName());
	
	private FrequentationService service = FrequentationService.getInstance();
	
	@GET
	@Path("/stats/week")
	public String statsByWeek() {
		JSONObject data = new JSONObject();
		JSONArray stats = new JSONArray();
		LOGGER.info("Begin: Stats by week");
		// Récuperation info
		Map<Date, List<FrequentationDTO>> week = service.getByDayOfWeek(0);
		Map<Date, List<FrequentationDTO>> lastWeek = service.getByDayOfWeek(1);
		// Creation statistique semaine courante + récupération clef
		final JSONArray labels = new JSONArray();
		final JSONArray value = new JSONArray();
		week.forEach((key, val) -> {
			labels.put(key.toString());
			value.put(val.stream().count());
		});
		JSONObject json = new JSONObject();
		json.put("title", "Semaine courante");
		json.put("value", value);
		json.put("color", new JSONObject("{ \"r\": 255, \"g\": 99, \"b\": 132 }"));
		stats.put(json);
		// Creation statistique semaine derniere
		final JSONArray valueLast = new JSONArray();
		lastWeek.forEach((key, val) -> {
			valueLast.put(val.stream().count());
		});
		json = new JSONObject();
		json.put("title", "Semaine derniere");
		json.put("value", valueLast);
		json.put("color", new JSONObject("{ \"r\": 235, \"g\": 192, \"b\": 52 }"));
		json.put("line", true);
		stats.put(json);
		LOGGER.info("End: Stats by week");
		data.put("title", "Frequentation de la semaine");
		data.put("labels", labels);
		data.put("stats", stats);
		return JsonUtils.success(data).toString();
	}
	
	@GET
	@Path("/stats/age")
	public String statsByAge() {
		JSONObject data = new JSONObject();
		JSONArray stats = new JSONArray();
		LOGGER.info("Begin: Stats by age");
		Map<String, Map<Integer, List<FrequentationDTO>>> freqs = service.getByAgeOnEspace(0);
		Map<Integer, Map<String, List<FrequentationDTO>>> tmp = new HashMap<>();
		// Pour chaque espace
		JSONArray labels = new JSONArray();
		List<String> espaces = new ArrayList<>();
		freqs.forEach((espace, freq) -> {
			labels.put(espace);
			espaces.add(espace);
		});
		freqs.forEach((espace, freq) -> {
			freq.forEach((age, f) -> {
				if (tmp.containsKey(age)) {
					Map<String, List<FrequentationDTO>> tmpMap = tmp.get(age);
					List<FrequentationDTO> tmpList = tmpMap.get(espace);
					f.forEach(elt -> tmpList.add(elt));
				} else {
					Map<String, List<FrequentationDTO>> tmpMap = new HashMap<>();
					espaces.forEach(elt -> tmpMap.put(elt, new ArrayList<>()));
					List<FrequentationDTO> tmpList = tmpMap.get(espace);
					f.forEach(elt -> tmpList.add(elt));
					tmp.put(age, tmpMap);
				}
			});
		});
		// Création des statistiques
		tmp.forEach((age, freq) -> {
			JSONObject json = new JSONObject();
			JSONArray value = new JSONArray();
			for(Entry<String, List<FrequentationDTO>> entry : freq.entrySet()) {
				value.put(entry.getValue().stream().count());
			}
			json.put("value", value);
			json.put("title", age + " ans");
			json.put("line", true);
			stats.put(json);
		});
		LOGGER.info("End: Stats by age");
		data.put("title", "Frequentation des espaces en fonction de l'age ce mois-ci");
		data.put("labels", labels);
		data.put("stats", stats);
		return JsonUtils.success(data).toString();
	}
	
	@GET
	@Path("/stats/espace/week")
	public String statsByEspaceWeek() {
		JSONObject data = new JSONObject();
		JSONArray stats = new JSONArray();
		LOGGER.info("Begin: Stats by espace week");
		// Recupération information
		Map<String, List<FrequentationDTO>> freqs = service.getByEspaceWeek(0);
		Map<String, List<FrequentationDTO>> freqsLast = service.getByEspaceWeek(1);
		// Statistique mois courant + récupération clef
		final JSONArray labels = new JSONArray();
		final JSONArray value = new JSONArray();
		freqs.forEach((key, val) -> {
			labels.put(key);
			value.put(val.stream().count());
		});
		JSONObject json = new JSONObject();
		json.put("title", "Semaine courante");
		json.put("value", value);
		json.put("color", new JSONObject("{ \"r\": 255, \"g\": 99, \"b\": 132 }"));
		stats.put(json);
		// Creation statistique semaine derniere
		final JSONArray valueLast = new JSONArray();
		freqsLast.forEach((key, val) -> {
			valueLast.put(val.stream().count());
		});
		json = new JSONObject();
		json.put("title", "Semaine derniere");
		json.put("value", valueLast);
		json.put("color", new JSONObject("{ \"r\": 235, \"g\": 192, \"b\": 52 }"));
		stats.put(json);
		LOGGER.info("End: Stats by espace week");
		data.put("title", "Frequentation des espace les deux dernieres semaines");
		data.put("labels", labels);
		data.put("stats", stats);
		return JsonUtils.success(data).toString();
	}
	
	@GET
	@Path("/stats/espace/month")
	public String statsByEspaceMonth() {
		JSONObject data = new JSONObject();
		JSONArray stats = new JSONArray();
		LOGGER.info("Begin: Stats by espace month");
		// Recupération information
		Map<String, List<FrequentationDTO>> freqs = service.getByEspaceMonth(0);
		Map<String, List<FrequentationDTO>> freqsLast = service.getByEspaceMonth(1);
		// Statistique mois courant + récupération clef
		final JSONArray labels = new JSONArray();
		final JSONArray value = new JSONArray();
		freqs.forEach((key, val) -> {
			labels.put(key);
			value.put(val.stream().count());
		});
		JSONObject json = new JSONObject();
		json.put("title", "Mois courante");
		json.put("value", value);
		json.put("color", new JSONObject("{ \"r\": 255, \"g\": 99, \"b\": 132 }"));
		stats.put(json);
		// Creation statistique semaine derniere
		final JSONArray valueLast = new JSONArray();
		freqsLast.forEach((key, val) -> {
			valueLast.put(val.stream().count());
		});
		json = new JSONObject();
		json.put("title", "Mois dernier");
		json.put("value", valueLast);
		json.put("color", new JSONObject("{ \"r\": 235, \"g\": 192, \"b\": 52 }"));
		stats.put(json);
		LOGGER.info("End: Stats by espace month");
		data.put("title", "Frequentation des espace les deux derniers mois");
		data.put("labels", labels);
		data.put("stats", stats);
		return JsonUtils.success(data).toString();
	}
	
}
