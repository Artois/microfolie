package microfolie.entry.rest;

import java.util.logging.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.json.JSONObject;

import microfolie.service.EspaceService;
import microfolie.service.FrequentationService;
import microfolie.service.UsagerService;
import microfolie.utils.JsonUtils;

@Path("/badge")
@Produces("application/json")
public class BadgeController {
	
	private static final Logger LOGGER = Logger.getLogger(BadgeController.class.getName());
	
	private UsagerService usagerService = UsagerService.getInstance();
	private EspaceService espaceService = EspaceService.getInstance();
	private FrequentationService service = FrequentationService.getInstance();
	
	@POST
	@Path("/{espace}/{code}")
	public String scanner(@PathParam("espace") String espace, @PathParam("code") String code) {
		LOGGER.info("Begin: Badge scanner (espace: " + espace + ", code: " + code + ")");
		JSONObject result;
		// Verification que les codes existes
		if (!usagerService.exist(code)) {
			result = JsonUtils.error("L'usager est introuvable");
		} else if (!espaceService.exist(espace)) {
			result = JsonUtils.error("L'espace est introuvable");
		} else {
			// Ajoute la fréquentation en base
			JSONObject data = new JSONObject();
			if(service.add(code, espace)) {
				data.put("msg", "Bienvenue dans l'espace #ESPACE#, votre code a bien &eacute;t&eacute; scann&eacute;");
			} else {
				data.put("msg", "Bon retour dans l'espace #ESPACE#, votre code a bien &eacute;t&eacute; scann&eacute;");
			}
			result = JsonUtils.success(data);
		}
		LOGGER.info("End: Badge scanner");
		return result.toString();
	}

}
