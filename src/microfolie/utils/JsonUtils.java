package microfolie.utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonUtils {
	
	public static JSONObject error(String message) {
		JSONObject json = new JSONObject();
		json.put("success", false);
		json.put("data", message);
		return json;
	}
	
	public static JSONObject success(JSONObject data) {
		JSONObject json = new JSONObject();
		json.put("success", true);
		json.put("data", data);
		return json;
	}
	
	public static JSONObject success(JSONArray data) {
		JSONObject json = new JSONObject();
		json.put("success", true);
		json.put("data", data);
		return json;
	}

}
