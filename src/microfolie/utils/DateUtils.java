package microfolie.utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class DateUtils extends org.apache.commons.lang3.time.DateUtils {
	
	public static final int BEGIN_DATE = 0;
	public static final int END_DATE = 1;
	
	/**
	 * Compare deux dates
	 * @param date1
	 * @param date2
	 * @return 0 si les dates sont égales, < 0 si date1 est avant date2, > 0 si date1 est apres date2
	 */
	public static long compareDate(Date date1, Date date2) {
		Calendar cal1 = removeTimeInformation(date1);
		long time1 = cal1.getTimeInMillis();
		Calendar cal2 = removeTimeInformation(date2);
		long time2 = cal2.getTimeInMillis();
		// Comparaison
		return time1 - time2;
	}

	public static Date[] getDateOfTheWeek() {
		return getDateOfTheWeek(0);
	}
	
	/**
	 * Donne la debute de debut et de fin d'une semaine
	 * @param history Pour revenir de n semaine en arriere, 
	 * exemple avec la valeur 1 récupère les dates de la semaine passée
	 * @return
	 */
	public static Date[] getDateOfTheWeek(int history) {
		Calendar c = GregorianCalendar.getInstance(Locale.FRANCE);
		c = removeTimeInformation(c);
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.add(Calendar.DATE, -7 * history);
		Date begin = c.getTime();
		c.add(Calendar.DATE, 6);
		Date end = c.getTime();
		return new Date[]{begin, end};
	}
	
	public static Date[] getAllDateOfTheWeek(int history) {
		Calendar c = GregorianCalendar.getInstance(Locale.FRANCE);
		c = removeTimeInformation(c);
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.add(Calendar.DATE, -7 * history);
		// Récuperation date
		Date[] result = new Date[7];
		for(int i = 0; i < 7; i++) {
			result[i] = c.getTime();
			c.add(Calendar.DATE, 1);
		}
		return result;
	}
	
	public static Date[] getDateOfTheMonth(int history) {
		Calendar c = GregorianCalendar.getInstance(Locale.FRANCE);
		c = removeTimeInformation(c);
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.MONTH, -1 * history);
		Date begin = c.getTime();
		c.add(Calendar.MONTH, 1);
		c.add(Calendar.DATE, -1);
		Date end = c.getTime();
		return new Date[]{begin, end};
	}
	
	public static int getAge(Date naissance) {
		Date aujourdhui = new Date();
		Date anniv = new Date(naissance.getTime());
		LocalDate localAujourdhui = aujourdhui.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate localNaissance = anniv.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		Period p = Period.between(localNaissance, localAujourdhui);
		return p.getYears();
	}
	
	public static Calendar removeTimeInformation(Date date) {
		Calendar c = GregorianCalendar.getInstance(Locale.FRANCE);
		c.setTime(date);
		return removeTimeInformation(c);
	}
	
	public static Calendar removeTimeInformation(Calendar c) {
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}
	
}
