package microfolie.persistance.entity;

import db.Persistable;
import db.annotation.DbField;
import db.annotation.DbId;

public class Niveau implements Persistable {
	
	@DbId
	@DbField("ID")
	public long id;
	
	@DbField("LIBELLE")
	public String libelle;

	@Override
	public long getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return libelle;
	}

}
