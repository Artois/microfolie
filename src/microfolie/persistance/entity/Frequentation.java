package microfolie.persistance.entity;

import java.util.Date;

import db.Persistable;
import db.annotation.DbField;
import db.annotation.DbId;
import db.annotation.DbLink;

public class Frequentation implements Persistable {

	@DbId
	@DbField("ID")
	public long id;
	
	@DbLink
	@DbField("ESPACE")
	public Espace espace;
	
	@DbLink
	@DbField("USAGER")
	public Usager usager;
	
	@DbField("DATE")
	public Date date;
	
	@Override
	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "(" + date + ") " + usager + " -> " + espace;
	}
	
}
