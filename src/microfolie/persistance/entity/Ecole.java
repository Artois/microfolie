package microfolie.persistance.entity;

import db.Persistable;
import db.annotation.DbField;
import db.annotation.DbId;
import db.annotation.DbLink;

public class Ecole implements Persistable {
	
	@DbId
	@DbField("ID")
	public long id;
	
	@DbField("LIBELLE")
	public String libelle;
	
	@DbLink
	@DbField("VILLE")
	public Ville ville;
	
	@DbLink
	@DbField("NIVEAU")
	public Niveau niveau;

	@Override
	public long getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return libelle + " (" + ville + ") : " + niveau;
	}

}
