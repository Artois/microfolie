package microfolie.persistance.entity;

import db.Persistable;
import db.annotation.DbField;
import db.annotation.DbId;

public class Espace implements Persistable {
	
	@DbId
	@DbField("ID")
	public long id;
	
	@DbField("LIBELLE")
	public String libelle;
	
	@DbField("CODE")
	public String code;

	@Override
	public long getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return libelle;
	}

}
