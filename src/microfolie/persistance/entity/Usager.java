package microfolie.persistance.entity;

import java.util.Date;

import db.Persistable;
import db.annotation.DbField;
import db.annotation.DbId;
import db.annotation.DbLink;

public class Usager implements Persistable {
	
	@DbId
	@DbField("ID")
	public long id;
	
	@DbField("NOM")
	public String nom;
	
	@DbField("PRENOM")
	public String prenom;
	
	@DbField("GENRE")
	public String genre;
	
	@DbField("DATEDENAISSANCE")
	public Date dateNaiss;
	
	@DbField("EMAIL")
	public String email;
	
	@DbField("TELEPHONE")
	public String telephone;
	
	@DbField("CONTACTURGENCE")
	public String contactUrgence;
	
	@DbField("CODE")
	public String code;
	
	@DbField("SCOLAIRE")
	public boolean scolaire;
	
	@DbField("DESCRIPTION")
	public String description;
	
	@DbLink
	@DbField("VILLE")
	public Ville ville;
	
	@DbLink
	@DbField("ECOLE")
	public Ecole ecole;

	@Override
	public long getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return prenom + " " + nom + " (" + code + ")";
	}

}
