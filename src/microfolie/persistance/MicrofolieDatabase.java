package microfolie.persistance;

import java.util.List;

import db.DatabaseManager;

public class MicrofolieDatabase extends DatabaseManager {
	
	private static MicrofolieDatabase instance;

	@Override
	public List<String> create() {
		return readSQLFile("/sql/create.sql");
	}

	@Override
	public List<String> drop() {
		return readSQLFile("/sql/drop.sql");
	}

	@Override
	public List<String> content() {
		return readSQLFile("/sql/insert.sql");
	}
	
	public static MicrofolieDatabase getInstance() {
		if (instance == null) {
			instance = new MicrofolieDatabase();
		}
		return instance;
	}
	
}
