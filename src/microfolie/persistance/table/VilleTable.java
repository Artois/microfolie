package microfolie.persistance.table;

import java.util.List;
import java.util.Optional;

import db.DatabaseTable;
import db.annotation.DbTable;
import microfolie.persistance.entity.Ville;

@DbTable(name = "Ville", entity = Ville.class)
public class VilleTable extends DatabaseTable<Ville> {
	
	private static VilleTable instance;
	
	private VilleTable() {
		// Private constructor for singleton
	}
	
	public Ville getByLibelle(String lib) {
		List<Ville> list = getByField("LIBELLE", lib);
		if(list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	public Optional<Ville> findByLibelle(String lib) {
		Ville obj = getByLibelle(lib);
		if (obj == null) {
			return Optional.empty();
		}
		return Optional.of(obj);
	}
	
	public Ville getByCodePostal(String cp) {
		List<Ville> list = getByField("CODEPOSTAL", cp);
		if(list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	public Optional<Ville> findByCodePostal(String cp) {
		Ville obj = getByCodePostal(cp);
		if (obj == null) {
			return Optional.empty();
		}
		return Optional.of(obj);
	}
	
	public static VilleTable getInstance() {
		if(instance == null) {
			instance = new VilleTable();
		}
		return instance;
	}

}
