package microfolie.persistance.table;

import java.util.List;
import java.util.Optional;

import db.DatabaseTable;
import db.annotation.DbTable;
import microfolie.persistance.entity.Niveau;

@DbTable(name = "Niveau", entity = Niveau.class)
public class NiveauTable extends DatabaseTable<Niveau> {
	
	private static NiveauTable instance;
	
	private NiveauTable() {
		// Private constructor for singleton
	}
	
	public Niveau getByLibelle(String lib) {
		List<Niveau> list = getByField("LIBELLE", lib);
		if(list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	public Optional<Niveau> findByLibelle(String lib) {
		Niveau obj = getByLibelle(lib);
		if (obj == null) {
			return Optional.empty();
		}
		return Optional.of(obj);
	}
	
	public static NiveauTable getInstance() {
		if(instance == null) {
			instance = new NiveauTable();
		}
		return instance;
	}
	
}
