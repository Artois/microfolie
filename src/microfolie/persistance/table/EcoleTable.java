package microfolie.persistance.table;

import java.util.List;
import java.util.Optional;

import db.DatabaseTable;
import db.annotation.DbTable;
import microfolie.persistance.entity.Ecole;

@DbTable(name = "Ecole", entity = Ecole.class)
public class EcoleTable extends DatabaseTable<Ecole> {
	
	private static EcoleTable instance;
	
	private EcoleTable() {
		// Private constructor for singleton
	}
	
	public Ecole getByLibelle(String lib) {
		List<Ecole> list = getByField("LIBELLE", lib);
		if(list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	public Optional<Ecole> findByLibelle(String lib) {
		Ecole obj = getByLibelle(lib);
		if (obj == null) {
			return Optional.empty();
		}
		return Optional.of(obj);
	}
	
	public static EcoleTable getInstance() {
		if(instance == null) {
			instance = new EcoleTable();
		}
		return instance;
	}

}
