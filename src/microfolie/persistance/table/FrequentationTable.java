package microfolie.persistance.table;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import db.Database;
import db.DatabaseTable;
import db.SQLQueryBuilder;
import db.annotation.DbTable;
import db.mapper.DatabaseMapper;
import microfolie.persistance.entity.Espace;
import microfolie.persistance.entity.Frequentation;
import microfolie.persistance.entity.Usager;
import microfolie.utils.DateUtils;

@DbTable(name = "Frequentation", entity = Frequentation.class)
public class FrequentationTable extends DatabaseTable<Frequentation>{

	private static FrequentationTable instance;
	
	private FrequentationTable() {
		// Private constructor for singleton
	}

	public List<Frequentation> getByUsagerAndEspace(long usagerId, long espaceId) {
		List<String> fields = new ArrayList<>();
		List<Object> values = new ArrayList<>();
		fields.add("USAGER");
		values.add(usagerId);
		fields.add("ESPACE");
		values.add(espaceId);
		List<Frequentation> frequentation = getWhere(fields, values);
		return frequentation;
	}
	
	public Frequentation getByUsagerAndEspaceToday(long usagerId, long espaceId) {
		List<String> fields = new ArrayList<>();
		List<Object> values = new ArrayList<>();
		fields.add("USAGER");
		values.add(usagerId);
		fields.add("ESPACE");
		values.add(espaceId);
		fields.add("DATE");
		values.add(DateUtils.removeTimeInformation(new Date()).getTime());
		List<Frequentation> frequentation = getWhere(fields, values);
		if (frequentation.size() <= 0) {
			return null;
		}
		return frequentation.get(0);
		
		/*SQLQueryBuilder sqlBuilder = SQLQueryBuilder.selectQuery("Frequentation");
		sqlBuilder.add("USAGER", usagerId);
		sqlBuilder.add("ESPACE", espaceId);
		sqlBuilder.add("DATE", );
		List<Object> params = sqlBuilder.getParams();
		params.add(from);
		params.add(to);
		Optional<List<Frequentation>> opt = Database.query(sql, params, DatabaseMapper.listMapper(getClass().getAnnotation(DbTable.class)));
		if (opt.isPresent()) {
			return cache(opt.get().get(0));
		}
		return null;*/
	}
	
	public List<Frequentation> getByDate(Date from, Date to) {
		SQLQueryBuilder sqlBuilder = SQLQueryBuilder.selectQuery("Frequentation");
		String sql = sqlBuilder.getSQL("And DATE >= ? And DATE <= ? Order By DATE");
		List<Object> params = sqlBuilder.getParams();
		params.add(from);
		params.add(to);
		Optional<List<Frequentation>> opt = Database.query(sql, params, DatabaseMapper.listMapper(getClass().getAnnotation(DbTable.class)));
		if (opt.isPresent()) {
			return cache(opt.get());
		}
		return new ArrayList<>();
	}
	
	public List<Frequentation> getByEspaceAndDate(Espace espace, Date from, Date to) {
		SQLQueryBuilder sqlBuilder = SQLQueryBuilder.selectQuery("Frequentation");
		sqlBuilder.add("ESPACE", espace.id);
		String sql = sqlBuilder.getSQL("And DATE >= ? And DATE <= ? Order By DATE");
		List<Object> params = sqlBuilder.getParams();
		params.add(from);
		params.add(to);
		Optional<List<Frequentation>> opt = Database.query(sql, params, DatabaseMapper.listMapper(getClass().getAnnotation(DbTable.class)));
		if (opt.isPresent()) {
			return cache(opt.get());
		}
		return new ArrayList<>();
	}
	
	public List<Frequentation> getByUsager(Usager usager) {
		SQLQueryBuilder sql = SQLQueryBuilder.selectQuery("Frequentation");
		sql.add("USAGER", usager.id);
		Optional<List<Frequentation>> opt = Database.query(sql.toSQL(), sql.getParams(), DatabaseMapper.listMapper(getClass().getAnnotation(DbTable.class)));
		if (opt.isPresent()) {
			return cache(opt.get());
		}
		return new ArrayList<>();
	}
	
	public static FrequentationTable getInstance() {
		if(instance == null) {
			instance = new FrequentationTable();
		}
		return instance;
	}	
	
}
