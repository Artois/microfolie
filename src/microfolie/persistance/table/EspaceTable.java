package microfolie.persistance.table;

import java.util.List;
import java.util.Optional;

import db.DatabaseTable;
import db.annotation.DbTable;
import microfolie.persistance.entity.Espace;

@DbTable(name = "Espace", entity = Espace.class)
public class EspaceTable extends DatabaseTable<Espace>{
	
	private static EspaceTable instance;
	
	private EspaceTable() {
		// Private constructor for singleton
	}
	
	public Espace getByCode(String code) {
		List<Espace> list = getByField("CODE", code);
		if(list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	public Optional<Espace> findByCode(String code) {
		Espace obj = getByCode(code);
		if (obj == null) {
			return Optional.empty();
		}
		return Optional.of(obj);
	}
	
	public static EspaceTable getInstance() {
		if(instance == null) {
			instance = new EspaceTable();
		}
		return instance;
	}

}
