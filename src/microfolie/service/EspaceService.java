package microfolie.service;

import java.util.List;

import microfolie.persistance.entity.Espace;
import microfolie.persistance.table.EspaceTable;
import microfolie.service.dto.EspaceDTO;
import microfolie.service.transformer.EspaceTransformer;

public class EspaceService {
	
	private static EspaceService instance;
	
	private EspaceTable table = EspaceTable.getInstance();
	
	private EspaceService() {
		// Constructeur privé pour singleton
	}
	
	public boolean exist(String code) {
		return table.findByCode(code).isPresent();
	}
	
	public List<EspaceDTO> getAll() {
		List<Espace> espaces = table.getAll();
		return EspaceTransformer.entityToDto(espaces);
	}
	
	public EspaceDTO getByCode(String code) {
		Espace espace = table.getByCode(code);
		return EspaceTransformer.entityToDto(espace);
	}
	
	public static EspaceService getInstance() {
		if (instance == null) {
			instance = new EspaceService();
		}
		return instance;
	}

}
