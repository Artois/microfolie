package microfolie.service.transformer;

import java.util.ArrayList;
import java.util.List;

import microfolie.persistance.entity.Ville;
import microfolie.service.dto.VilleDTO;

public class VilleTransformer {
	
	public static Ville dtoToEntity(VilleDTO dto) {
		Ville ville = new Ville();
		ville.cp = dto.getCp();
		ville.libelle = dto.getLibelle();
		return ville;
	}
	
	public static List<Ville> dtoToEntity(List<VilleDTO> dto) {
		List<Ville> result = new ArrayList<>();
		dto.forEach(elt -> result.add(dtoToEntity(elt)));
		return result;
	}
	
	public static VilleDTO entityToDto(Ville ville) {
		VilleDTO dto = new VilleDTO();
		dto.setCp(ville.cp);
		dto.setLibelle(ville.libelle);
		return dto;
	}
	
	public static List<VilleDTO> entityToDto(List<Ville> ville) {
		List<VilleDTO> result = new ArrayList<>();
		ville.forEach(elt -> result.add(entityToDto(elt)));
		return result;
	}

}
