package microfolie.service.transformer;

import java.util.ArrayList;
import java.util.List;

import microfolie.persistance.entity.Espace;
import microfolie.service.dto.EspaceDTO;

public class EspaceTransformer {
	
	public static Espace dtoToEntity(EspaceDTO dto) {
		Espace espace = new Espace();
		espace.code = dto.getCode();
		espace.libelle = dto.getLibelle();
		return espace;
	}
	
	public static List<Espace> dtoToEntity(List<EspaceDTO> dto) {
		List<Espace> result = new ArrayList<>();
		dto.forEach(elt -> result.add(dtoToEntity(elt)));
		return result;
	}
	
	public static EspaceDTO entityToDto(Espace espace) {
		EspaceDTO dto = new EspaceDTO();
		dto.setCode(espace.code);
		dto.setLibelle(espace.libelle);
		return dto;
	}
	
	public static List<EspaceDTO> entityToDto(List<Espace> espace) {
		List<EspaceDTO> result = new ArrayList<>();
		espace.forEach(elt -> result.add(entityToDto(elt)));
		return result;
	}

}
