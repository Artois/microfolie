package microfolie.service.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import microfolie.persistance.entity.Ecole;
import microfolie.persistance.entity.Niveau;
import microfolie.persistance.entity.Ville;
import microfolie.persistance.table.NiveauTable;
import microfolie.persistance.table.VilleTable;
import microfolie.service.dto.EcoleDTO;

public class EcoleTransformer {
	
	private static VilleTable villeTable = VilleTable.getInstance();
	private static NiveauTable niveauTable = NiveauTable.getInstance();
	
	public static Ecole dtoToEntity(EcoleDTO dto) {
		Ecole ecole = new Ecole();
		ecole.libelle = dto.getLibelle();
		Optional<Ville> optVille = villeTable.findByLibelle(dto.getVille());
		Optional<Niveau> optNiveau = niveauTable.findByLibelle(dto.getNiveau());
		if (optVille.isPresent()) {
			ecole.ville = optVille.get();
		}
		if (optNiveau.isPresent()) {
			ecole.niveau = optNiveau.get();
		}
		return ecole;
	}
	
	public static List<Ecole> dtoToEntity(List<EcoleDTO> dto) {
		List<Ecole> result = new ArrayList<>();
		dto.forEach(elt -> result.add(dtoToEntity(elt)));
		return result;
	}
	
	public static EcoleDTO entityToDto(Ecole ecole) {
		EcoleDTO dto = new EcoleDTO();
		dto.setLibelle(ecole.libelle);
		if (ecole.ville != null) {
			dto.setVille(ecole.ville.libelle);
		}
		if (ecole.niveau != null) {
			dto.setNiveau(ecole.niveau.libelle);
		}
		return dto;
	}
	
	public static List<EcoleDTO> entityToDto(List<Ecole> ecole) {
		List<EcoleDTO> result = new ArrayList<>();
		ecole.forEach(elt -> result.add(entityToDto(elt)));
		return result;
	}

}
