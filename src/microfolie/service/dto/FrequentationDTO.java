package microfolie.service.dto;

import java.util.Date;

public class FrequentationDTO {
	
	private String usager;
	private String usagerCode;
	private int age;
	private String espace;
	private String espaceCode;
	private Date date;
	
	public FrequentationDTO() {
		// Constructeur par defaut pour contruire un objet vide
	}
	
	public FrequentationDTO(String usager, String usagerCode, int age, String espace, String espaceCode, Date date) {
		this.usager = usager;
		this.usagerCode = usagerCode;
		this.age = age;
		this.espace = espace;
		this.espaceCode = espaceCode;
		this.date = date;
	}

	public String getUsager() {
		return usager;
	}

	public void setUsager(String usager) {
		this.usager = usager;
	}
	
	public String getUsagerCode() {
		return usagerCode;
	}
	
	public void setUsagerCode(String usagerCode) {
		this.usagerCode = usagerCode;
	}
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String getEspace() {
		return espace;
	}

	public void setEspace(String espace) {
		this.espace = espace;
	}

	public String getEspaceCode() {
		return espaceCode;
	}
	
	public void setEspaceCode(String espaceCode) {
		this.espaceCode = espaceCode;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
