package microfolie.service.dto;

public class VilleDTO {
	
	private String libelle;
	private String cp;
	
	public VilleDTO() {
		// Constructeur par defaut pour contruire un objet vide
	}
	
	public VilleDTO(String libelle, String cp) {
		super();
		this.libelle = libelle;
		this.cp = cp;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}
	
}
