package microfolie.service.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class UsagerDTO {

	private String nom;
	private String prenom;
	private String genre;
	private Date dateNaissance;
	private String ville;
	private String situation;
	private String email;
	private String telephone;
	private String contactUrgence;
	private String code;
	
	public UsagerDTO() {
		// Constructeur par defaut pour contruire un objet vide
	}
	
	public UsagerDTO(JSONObject json) {
		this.nom = json.getString("nom");
		this.prenom = json.getString("prenom");
		this.genre = json.getString("genre");
		try {
			this.dateNaissance = new SimpleDateFormat("dd/MM/yyyy").parse(json.getString("dateNaissance"));
		} catch (JSONException | ParseException e) {
			this.dateNaissance = null;
		}
		this.ville = json.getString("ville");
		this.situation = json.getString("situation");
		this.email = json.getString("email");
		this.telephone = json.getString("telephone");
		this.contactUrgence = json.getString("contactUrgence");
		this.code = json.getString("code");
	}
	
	public UsagerDTO(String nom, String prenom, String genre, Date dateNaissance, String ville, String situation,
			String email, String telephone, String contactUrgence) {
		this.nom = nom;
		this.prenom = prenom;
		this.genre = genre;
		this.dateNaissance = dateNaissance;
		this.ville = ville;
		this.situation = situation;
		this.email = email;
		this.telephone = telephone;
		this.contactUrgence = contactUrgence;
	}
	
	public UsagerDTO(String nom, String prenom, String genre, Date dateNaissance, String ville, String situation,
			String email, String telephone, String contactUrgence, String code) {
		this.nom = nom;
		this.prenom = prenom;
		this.genre = genre;
		this.dateNaissance = dateNaissance;
		this.ville = ville;
		this.situation = situation;
		this.email = email;
		this.telephone = telephone;
		this.contactUrgence = contactUrgence;
		this.code = code;
	}

	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public Date getDateNaissance() {
		return dateNaissance;
	}
	
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	
	public String getVille() {
		return ville;
	}
	
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	public String getSituation() {
		return situation;
	}
	
	public void setSituation(String situation) {
		this.situation = situation;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTelephone() {
		return telephone;
	}
	
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getContactUrgence() {
		return contactUrgence;
	}
	
	public void setContactUrgence(String contactUrgence) {
		this.contactUrgence = contactUrgence;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
}
