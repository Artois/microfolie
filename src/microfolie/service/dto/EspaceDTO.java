package microfolie.service.dto;

public class EspaceDTO {
	
	private String libelle;
	private String code;
	
	public EspaceDTO() {
		// Constructeur par defaut pour contruire un objet vide
	}
	
	public EspaceDTO(String libelle, String code) {
		this.libelle = libelle;
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
