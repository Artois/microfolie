package microfolie.service.dto;

public class EcoleDTO {
	
	private String libelle;
	private String ville;
	private String niveau;
	
	public EcoleDTO() {
		// Constructeur par defaut pour contruire un objet vide
	}

	public EcoleDTO(String libelle, String ville, String niveau) {
		super();
		this.libelle = libelle;
		this.ville = ville;
		this.niveau = niveau;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

}
