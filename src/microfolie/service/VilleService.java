package microfolie.service;

import java.util.List;

import microfolie.persistance.entity.Ville;
import microfolie.persistance.table.VilleTable;
import microfolie.service.dto.VilleDTO;
import microfolie.service.transformer.VilleTransformer;

public class VilleService {
	
	private static VilleService instance;
	
	private VilleTable table = VilleTable.getInstance();

	private VilleService() {
		// Constructeur privé pour singleton
	}
	
	public List<VilleDTO> getAll() {
		List<Ville> villes = table.getAll();
		return VilleTransformer.entityToDto(villes);
	}
	
	public static VilleService getInstance() {
		if (instance == null) {
			instance = new VilleService();
		}
		return instance;
	}
	
}
