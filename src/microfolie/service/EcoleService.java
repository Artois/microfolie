package microfolie.service;

import java.util.List;

import microfolie.persistance.entity.Ecole;
import microfolie.persistance.table.EcoleTable;
import microfolie.service.dto.EcoleDTO;
import microfolie.service.transformer.EcoleTransformer;

public class EcoleService {
	
	private static EcoleService instance;
	
	private EcoleTable table = EcoleTable.getInstance();
	
	private EcoleService() {
		// Constructeur privé pour singleton
	}
	
	public List<EcoleDTO> getAll() {
		List<Ecole> list = table.getAll();
		return EcoleTransformer.entityToDto(list);
	}
	
	public static EcoleService getInstance() {
		if (instance == null) {
			instance = new EcoleService();
		}
		return instance;
	}

}
